﻿using JustClassifieds.Domain.Accounts;
using JustClassifieds.Domain.Ads;
using JustClassifieds.Domain.Articles;
using JustClassifieds.Domain.Resources;
using System.Data.Entity;

namespace JustClassifieds.Application.Interfaces
{
    public interface IDatabaseService
    {
        IDbSet<Category> Categories { get; set; }
        IDbSet<Language> Languages { get; set; }
        IDbSet<Translation> Translations { get; set; }
        IDbSet<Region> Regions { get; set; }
        IDbSet<Currency> Currencies { get; set; }
        IDbSet<ApplicationUser> ApplicationUsers { get; set; }
        IDbSet<Article> Articles { get; set; }
        IDbSet<Ad> Ads { get; set; }
        void Save();
    }
}
