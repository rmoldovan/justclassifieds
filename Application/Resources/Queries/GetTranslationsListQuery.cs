﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries
{
    public class GetTranslationsListQuery : IGetTranslationsListQuery
    {
        private readonly IDatabaseService _database;

        public GetTranslationsListQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<TranslationModel> Execute()
        {
            var categories = _database.Translations.Include(t => t.Language)
                .Select(t => new TranslationModel(t));

            return categories.ToList();
        }
    }
}
