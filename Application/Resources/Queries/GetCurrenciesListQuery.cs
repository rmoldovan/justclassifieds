﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries
{
    public class GetCurrenciesListQuery : IGetCurrenciesListQuery
    {
        private readonly IDatabaseService _database;

        public GetCurrenciesListQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<CurrencyModel> Execute()
        {
            var languages = _database.Currencies
                .Select(c => new CurrencyModel(c));

            return languages.ToList();
        }
    }
}
