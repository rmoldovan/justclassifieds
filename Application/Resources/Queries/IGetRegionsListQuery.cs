﻿using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;

namespace JustClassifieds.Application.Resources.Queries
{
    public interface IGetRegionsListQuery
    {
        List<RegionModel> Execute();
    }
}
