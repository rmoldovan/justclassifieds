﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries
{
    public class GetRegionsListQuery : IGetRegionsListQuery
    {
        private readonly IDatabaseService _database;

        public GetRegionsListQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<RegionModel> Execute()
        {
            var categories = _database.Regions
                .Select(r => new RegionModel(r));

            return categories.ToList();
        }
    }
}
