﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries
{
    public class GetLanguagesListQuery : IGetLanguagesListQuery
    {
        private readonly IDatabaseService _database;

        public GetLanguagesListQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<LanguageModel> Execute()
        {
            var languages = _database.Languages
                .Select(l => new LanguageModel(l));

            return languages.ToList();
        }
    }
}
