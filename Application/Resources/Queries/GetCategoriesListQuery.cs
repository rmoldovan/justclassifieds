﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Application.Resources.Queries.Models;
using System.Collections.Generic;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries
{
    public class GetCategoriesListQuery : IGetCategoriesListQuery
    {
        private readonly IDatabaseService _database;

        public GetCategoriesListQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<CategoryModel> Execute()
        {
            var categories = _database.Categories
                .Select(c => new CategoryModel(c));

            return categories.ToList();
        }
    }
}
