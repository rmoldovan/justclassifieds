﻿using AutoMoq;
using JustClassifieds.Application.Interfaces;
using JustClassifieds.Common.Mocks;
using JustClassifieds.Domain.Resources;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries.Tests
{
    [TestFixture]
    public class GetCurrenciesListQueryTests
    {
        private GetCurrenciesListQuery _query;
        private AutoMoqer _mocker;
        private Currency _currency;

        private const int Id = 1;
        private const string Name = "Euro";
        private const string ShortName = "€";
        private const string OfficialName = "EUR";

        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMoqer();

            _currency = new Currency
            {
                Id = Id,
                Name = Name,
                ShortName = ShortName,
                OfficialName = OfficialName
            };

            _mocker.GetMock<IDbSet<Currency>>()
                .SetUpDbSet(new List<Currency> { _currency });

            _mocker.GetMock<IDatabaseService>()
                .Setup(p => p.Currencies)
                .Returns(_mocker.GetMock<IDbSet<Currency>>().Object);

            _query = _mocker.Create<GetCurrenciesListQuery>();
        }

        [Test]
        public void TestExecuteShouldReturnListOfCurrencies()
        {
            var results = _query.Execute();

            var result = results.Single();

            Assert.That(result.Id, Is.EqualTo(Id));
            Assert.That(result.Name, Is.EqualTo(Name));
            Assert.That(result.ShortName, Is.EqualTo(ShortName));
            Assert.That(result.OfficialName, Is.EqualTo(OfficialName));
        }
    }
}
