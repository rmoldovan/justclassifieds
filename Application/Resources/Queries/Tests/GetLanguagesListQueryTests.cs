﻿using AutoMoq;
using JustClassifieds.Application.Interfaces;
using JustClassifieds.Common.Mocks;
using JustClassifieds.Domain.Resources;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries.Tests
{
    [TestFixture]
    public class GetLanguagesListQueryTests
    {
        private GetLanguagesListQuery _query;
        private AutoMoqer _mocker;
        private Language _language;

        private const int Id = 1;
        private const string LanguageCultureName = "ro-RO";
        private const string DisplayName = "Romanian - Romania";

        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMoqer();

            _language = new Language
            {
                Id = Id,
                LanguageCultureName = LanguageCultureName,
                DisplayName = DisplayName
            };

            _mocker.GetMock<IDbSet<Language>>()
                .SetUpDbSet(new List<Language> { _language });

            _mocker.GetMock<IDatabaseService>()
                .Setup(p => p.Languages)
                .Returns(_mocker.GetMock<IDbSet<Language>>().Object);

            _query = _mocker.Create<GetLanguagesListQuery>();
        }

        [Test]
        public void TestExecuteShouldReturnListOfLanguages()
        {
            var results = _query.Execute();

            var result = results.Single();

            Assert.That(result.Id, Is.EqualTo(Id));
            Assert.That(result.LanguageCultureName, Is.EqualTo(LanguageCultureName));
            Assert.That(result.DisplayName, Is.EqualTo(DisplayName));
        }
    }
}
