﻿using AutoMoq;
using JustClassifieds.Application.Interfaces;
using JustClassifieds.Common.Mocks;
using JustClassifieds.Domain.Resources;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries.Tests
{
    [TestFixture]
    public class GetRegionsListQueryTests
    {
        private GetRegionsListQuery _query;
        private AutoMoqer _mocker;
        private Region _region;

        private const int Id = 1;
        private const string Slug = "oradea";
        private const string Key = "region-oradea";
        private const bool IsCapital = true;
        private const double Latitude = 47.0722222;
        private const double Longitude = 21.9211111;

        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMoqer();

            _region = new Region
            {
                Id = Id,
                Slug = Slug,
                Key = Key,
                IsCapital = IsCapital,
                Latitude = Latitude,
                Longitude = Longitude
            };

            _mocker.GetMock<IDbSet<Region>>()
                .SetUpDbSet(new List<Region> { _region });

            _mocker.GetMock<IDatabaseService>()
                .Setup(p => p.Regions)
                .Returns(_mocker.GetMock<IDbSet<Region>>().Object);

            _query = _mocker.Create<GetRegionsListQuery>();
        }

        [Test]
        public void TestExecuteShouldReturnListOfRegions()
        {
            var results = _query.Execute();

            var result = results.Single();

            Assert.That(result.Id, Is.EqualTo(Id));
            Assert.That(result.Slug, Is.EqualTo(Slug));
            Assert.That(result.Key, Is.EqualTo(Key));
            Assert.That(result.IsCapital, Is.EqualTo(IsCapital));
            Assert.That(result.Latitude, Is.EqualTo(Latitude));
            Assert.That(result.Longitude, Is.EqualTo(Longitude));
        }
    }
}
