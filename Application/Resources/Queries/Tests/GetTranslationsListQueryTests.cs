﻿using AutoMoq;
using JustClassifieds.Application.Interfaces;
using JustClassifieds.Common.Mocks;
using JustClassifieds.Domain.Resources;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries.Tests
{
    [TestFixture]
    public class GetTranslationsListQueryTests
    {
        private GetTranslationsListQuery _query;
        private AutoMoqer _mocker;
        private Translation _translation;

        private const int Id = 1;
        private const string Key = "car-color-red";
        private const string Value = "rosu";
        private Language Language = new Language { Id = 1, LanguageCultureName = "ro-RO", DisplayName = "Romanian - Romania" };

        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMoqer();

            _translation = new Translation
            {
                Id = Id,
                Key = Key,
                Value = Value,
                Language = Language
            };

            _mocker.GetMock<IDbSet<Translation>>()
                .SetUpDbSet(new List<Translation> { _translation });

            _mocker.GetMock<IDatabaseService>()
                .Setup(p => p.Translations)
                .Returns(_mocker.GetMock<IDbSet<Translation>>().Object);

            _query = _mocker.Create<GetTranslationsListQuery>();
        }

        [Test]
        public void TestExecuteShouldReturnListOfTranslations()
        {
            var results = _query.Execute();

            var result = results.Single();

            Assert.That(result.Id, Is.EqualTo(Id));
            Assert.That(result.Key, Is.EqualTo(Key));
            Assert.That(result.Value, Is.EqualTo(Value));
            Assert.That(result.Language.Id, Is.EqualTo(Language.Id));
            Assert.That(result.Language.LanguageCultureName, Is.EqualTo(Language.LanguageCultureName));
            Assert.That(result.Language.DisplayName, Is.EqualTo(Language.DisplayName));
        }
    }
}
