﻿using AutoMoq;
using JustClassifieds.Application.Interfaces;
using JustClassifieds.Common.Mocks;
using JustClassifieds.Domain.Resources;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustClassifieds.Application.Resources.Queries.Tests
{
    [TestFixture]
    public class GetCategoriesListQueryTests
    {
        private GetCategoriesListQuery _query;
        private AutoMoqer _mocker;
        private Category _category;

        private const int Id = 1;
        private const string Slug = "auto";
        private const string Key = "category-auto";

        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMoqer();

            _category = new Category
            {
                Id = Id,
                Slug = Slug,
                Key = Key
            };

            _mocker.GetMock<IDbSet<Category>>()
                .SetUpDbSet(new List<Category> { _category });

            _mocker.GetMock<IDatabaseService>()
                .Setup(p => p.Categories)
                .Returns(_mocker.GetMock<IDbSet<Category>>().Object);

            _query = _mocker.Create<GetCategoriesListQuery>();
        }

        [Test]
        public void TestExecuteShouldReturnListOfCategories()
        {
            var results = _query.Execute();

            var result = results.Single();

            Assert.That(result.Id, Is.EqualTo(Id));
            Assert.That(result.Slug, Is.EqualTo(Slug));
            Assert.That(result.Key, Is.EqualTo(Key));
        }
    }
}
