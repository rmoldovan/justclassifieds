﻿using JustClassifieds.Domain.Resources;

namespace JustClassifieds.Application.Resources.Queries.Models
{
    public class RegionModel
    {
        public int Id { get; }
        public string Slug { get; }
        public string Key { get; }
        public bool IsCapital { get; }
        public double? Latitude { get; }
        public double? Longitude { get; }
        public virtual RegionModel ParentRegion { get; }

        public RegionModel(Region region)
        {
            Id = region.Id;
            Slug = region.Slug;
            Key = region.Key;
            IsCapital = region.IsCapital;
            Latitude = region.Latitude;
            Longitude = region.Longitude;
        }
    }
}
