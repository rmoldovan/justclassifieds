﻿using JustClassifieds.Domain.Resources;

namespace JustClassifieds.Application.Resources.Queries.Models
{
    public class TranslationModel
    {
        public int Id { get; }
        public string Key { get; }
        public string Value { get; }
        public virtual LanguageModel Language { get; }

        public TranslationModel(Translation translation)
        {
            Id = translation.Id;
            Key = translation.Key;
            Value = translation.Value;
            Language = new LanguageModel(translation.Language);
        }
    }
}
