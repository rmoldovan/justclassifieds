﻿using JustClassifieds.Domain.Resources;

namespace JustClassifieds.Application.Resources.Queries.Models
{
    public class CategoryModel
    {
        public int Id { get; }
        public string Slug { get; }
        public string Key { get; }
        public virtual CategoryModel ParentCategory { get; }

        public CategoryModel(Category category)
        {
            Id = category.Id;
            Slug = category.Slug;
            Key = category.Key;
        }
    }
}
