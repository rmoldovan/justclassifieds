﻿using JustClassifieds.Domain.Resources;

namespace JustClassifieds.Application.Resources.Queries.Models
{
    public class LanguageModel
    {
        public int Id { get; }
        public string LanguageCultureName { get; }
        public string DisplayName { get; }

        public LanguageModel(Language language)
        {
            Id = language.Id;
            LanguageCultureName = language.LanguageCultureName;
            DisplayName = language.DisplayName;
        }
    }
}
