﻿using JustClassifieds.Domain.Resources;

namespace JustClassifieds.Application.Resources.Queries.Models
{
    public class CurrencyModel
    {
        public int Id { get; }
        public string Name { get; }
        public string ShortName { get; }
        public string OfficialName { get; }

        public CurrencyModel(Currency currency)
        {
            Id = currency.Id;
            Name = currency.Name;
            ShortName = currency.ShortName;
            OfficialName = currency.OfficialName;
        }
    }
}
