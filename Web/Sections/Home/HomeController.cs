﻿using JustClassifieds.Web.MultiTenant;
using System.Web.Mvc;

namespace JustClassifieds.Web.Sections.Home
{
    public class HomeController : MultiTenantMvcController
    {
        public ViewResult Index()
        {
            ViewBag.Tenant = this.Tenant.Name;
            return View();
        }
    }
}