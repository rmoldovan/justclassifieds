﻿using System;
using System.Web;
using System.Web.Mvc;

namespace JustClassifieds.Web.MultiTenant
{
    public class MultiTenantMvcController : Controller
    {
        public Tenant Tenant
        {
            get
            {
                object multiTenant;
                if (!Request.GetOwinContext().Environment.TryGetValue("MultiTenant", out multiTenant))
                {
                    throw new ApplicationException("Could not find tenant");
                }
                return (Tenant)multiTenant;
            }
        }
    }
}