﻿using System.Configuration;

namespace JustClassifieds.Web.MultiTenant
{
    public class TenantsSettings : ConfigurationSection
    {
        private static TenantsSettings settings
            = ConfigurationManager.GetSection("tenantsSettings") as TenantsSettings;

        public static TenantsSettings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("tenants", IsRequired = true, IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(Tenants), AddItemName = "tenant")]
        public Tenants Tenants
        {
            get
            {
                object o = this["tenants"];
                return o as Tenants;
            }
        }
    }

    public class Tenants
        : ConfigurationElementCollection
    {
        public Tenant this[int index]
        {
            get
            {
                return base.BaseGet(index) as Tenant;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new Tenant this[string responseString]
        {
            get { return (Tenant)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override System.Configuration.ConfigurationElement CreateNewElement()
        {
            return new Tenant();
        }

        protected override object GetElementKey(System.Configuration.ConfigurationElement element)
        {
            return ((Tenant)element).Name;
        }
    }

    public class Tenant : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("domainName", IsRequired = true)]
        public string DomainName
        {
            get
            {
                return this["domainName"] as string;
            }
        }

        [ConfigurationProperty("default", IsRequired = true)]
        public string Default
        {
            get
            {
                return this["default"] as string;
            }
        }

        public bool IsDefault
        {
            get
            {
                return this.Default.Equals("true");
            }
        }
    }
}