﻿using System;
using System.Web;
using System.Web.Mvc;

namespace JustClassifieds.Web.MultiTenant
{
    public class MultiTenantViewEngine : RazorViewEngine
    {
        public MultiTenantViewEngine()
        {
            ViewLocationFormats = new[] {
            "~/Sections/{1}/Views/%1/{0}.cshtml",
            "~/Sections/{1}/Views/{0}.cshtml",
            "~/Sections/Shared/Views/%1/{0}.cshtml",
            "~/Sections/Shared/Views/{0}.cshtml"
            };

            MasterLocationFormats = new[] {
            "~/Sections/{1}/Views/%1/{0}.cshtml",
            "~/Sections/{1}/Views/{0}.cshtml",
            "~/Sections/Shared/Views/%1/{0}.cshtml",
            "~/Sections/Shared/Views/{0}.cshtml"
            };

            PartialViewLocationFormats = new[] {
            "~/Sections/{1}/Views/%1/{0}.cshtml",
            "~/Sections/{1}/Views/{0}.cshtml",
            "~/Sections/Shared/Views/%1/{0}.cshtml",
            "~/Sections/Shared/Views/{0}.cshtml"
            };
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            var passedController = controllerContext.Controller as MultiTenantMvcController;
            if (passedController != null)
                return base.CreatePartialView(controllerContext, partialPath.Replace("%1", passedController.Tenant.Name));
            else
                return base.CreatePartialView(controllerContext, partialPath);
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            var passedController = controllerContext.Controller as MultiTenantMvcController;
            if (passedController != null)
                return base.CreateView(controllerContext,
                    viewPath.Replace("%1", passedController.Tenant.Name),
                    masterPath.Replace("%1", passedController.Tenant.Name));
            else
                return base.CreateView(controllerContext, viewPath, masterPath);
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            try
            {

                var passedController = controllerContext.Controller as MultiTenantMvcController;
                if (passedController != null)
                {
                    return base.FileExists(controllerContext, virtualPath.Replace("%1", passedController.Tenant.Name));
                }
                else
                {
                    var newEx = new Exception("passedController is null, Controller must inherit MultiTenantMvcController");
                    return base.FileExists(controllerContext, virtualPath);
                }

            }
            catch (HttpException exception)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
                if (exception.GetHttpCode() != 0x194)
                {
                    throw;
                }
                return false;
            }
            catch (Exception exception)
            {
                var newEx = new Exception((controllerContext == null).ToString(), exception);
                //Elmah.ErrorSignal.FromCurrentContext().Raise(newEx);
                return false;
            }
        }
    }
}