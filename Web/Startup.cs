﻿using JustClassifieds.Web.MultiTenant;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(JustClassifieds.Web.Startup))]
namespace JustClassifieds.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (ctx, next) =>
            {
                Tenant tenant = GetTenantBasedOnUrl(ctx.Request.Uri.Host);
                if (tenant == null)
                {
                    throw new ApplicationException("tenant not found");
                }

                ctx.Environment.Add("MultiTenant", tenant);
                await next();
            });
        }

        private Tenant GetTenantBasedOnUrl(string urlHost)
        {
            if (String.IsNullOrEmpty(urlHost))
            {
                throw new ApplicationException("urlHost must be specified");
            }

            Tenant tenant = null;
            for (int i = 0; i < TenantsSettings.Settings.Tenants.Count; i++)
            {
                if (TenantsSettings.Settings.Tenants[i].DomainName.ToLower().Equals(urlHost))
                    tenant = TenantsSettings.Settings.Tenants[i];
                if (tenant == null && TenantsSettings.Settings.Tenants[i].IsDefault)
                    tenant = TenantsSettings.Settings.Tenants[i];
            }

            if (tenant == null && TenantsSettings.Settings.Tenants.Count > 0)
            {
                tenant = TenantsSettings.Settings.Tenants[0];
            }

            if (tenant == null)
            {
                throw new ApplicationException("tenant not found based on URL, no default found");
            }

            return tenant;
        }
    }
}