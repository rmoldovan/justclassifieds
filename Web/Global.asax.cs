﻿using JustClassifieds.Web.MultiTenant;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace JustClassifieds.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MultiTenantViewEngine());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("Mobile")
            {
                ContextCondition = (ctx =>
                 ctx.Request.UserAgent.IndexOf("iPad", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("iPhone", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("iPod", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Tablet", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Android", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Windows Phone", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Windows Mobile", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Phone", StringComparison.OrdinalIgnoreCase) >= 0
                 || ctx.Request.UserAgent.IndexOf("Mobile", StringComparison.OrdinalIgnoreCase) >= 0)
            });
        }
    }
}