﻿using JustClassifieds.Domain.Common;
using System;

namespace JustClassifieds.Domain.Accounts
{
    public class ApplicationUser : BasicEntity<Guid>
    {
        public string Email { get; set; }
    }
}
