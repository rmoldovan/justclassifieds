﻿using JustClassifieds.Domain.Common;
using System;

namespace JustClassifieds.Domain.Articles
{
    public class Article : BasicEntity<Guid>
    {
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
