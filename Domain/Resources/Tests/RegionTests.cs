﻿using NUnit.Framework;

namespace JustClassifieds.Domain.Resources.Tests
{
    [TestFixture]
    public class RegionTests
    {
        private readonly Region _region;
        private const int Id = 1;
        private const string Slug = "oradea";
        private const string Key = "region-oradea";
        private const bool IsCapital = true;
        private const double Latitude = 47.0722222;
        private const double Longitude = 21.9211111;

        public RegionTests()
        {
            _region = new Region();
        }

        [Test]
        public void TestSetAndGetId()
        {
            _region.Id = Id;
            Assert.That(_region.Id,
                Is.EqualTo(Id));
        }

        [Test]
        public void TestSetAndGetSlug()
        {
            _region.Slug = Slug;
            Assert.That(_region.Slug,
                Is.EqualTo(Slug));
        }

        [Test]
        public void TestSetAndGetKey()
        {
            _region.Key = Key;
            Assert.That(_region.Key,
                Is.EqualTo(Key));
        }

        [Test]
        public void TestSetAndGetIsCapital()
        {
            _region.IsCapital = IsCapital;
            Assert.That(_region.IsCapital,
                Is.EqualTo(IsCapital));
        }

        [Test]
        public void TestLatitudeAllowNull()
        {
            Assert.That(_region.Latitude,
                Is.Null);
        }

        [Test]
        public void TestLongitudeAllowNull()
        {
            Assert.That(_region.Longitude,
                Is.Null);
        }

        [Test]
        public void TestSetAndGetLatitude()
        {
            _region.Latitude = Latitude;
            Assert.That(_region.Latitude,
                Is.EqualTo(Latitude));
        }

        [Test]
        public void TestSetAndGetLongitude()
        {
            _region.Longitude = Longitude;
            Assert.That(_region.Longitude,
                Is.EqualTo(Longitude));
        }
    }
}
