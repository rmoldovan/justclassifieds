﻿using NUnit.Framework;

namespace JustClassifieds.Domain.Resources.Tests
{
    [TestFixture]
    public class CurrencyTests
    {
        private readonly Currency _currency;
        private const int Id = 1;
        private const string Name = "Euro";
        private const string ShortName = "€";
        private const string OfficialName = "EUR";

        public CurrencyTests()
        {
            _currency = new Currency();
        }

        [Test]
        public void TestSetAndGetId()
        {
            _currency.Id = Id;
            Assert.That(_currency.Id,
                Is.EqualTo(Id));
        }

        [Test]
        public void TestSetAndGetCurrencyName()
        {
            _currency.Name = Name;
            Assert.That(_currency.Name,
                Is.EqualTo(Name));
        }

        [Test]
        public void TestSetAndGetCurrencyShortName()
        {
            _currency.ShortName = ShortName;
            Assert.That(_currency.ShortName,
                Is.EqualTo(ShortName));
        }

        [Test]
        public void TestSetAndGetCurrencyOfficialName()
        {
            _currency.OfficialName = OfficialName;
            Assert.That(_currency.OfficialName,
                Is.EqualTo(OfficialName));
        }
    }
}
