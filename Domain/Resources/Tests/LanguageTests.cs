﻿using NUnit.Framework;

namespace JustClassifieds.Domain.Resources.Tests
{
    [TestFixture]
    public class LanguageTests
    {
        private readonly Language _language;
        private const int Id = 1;
        private const string LanguageCultureName = "ro-RO";
        private const string DisplayName = "Romanian - Romania";

        public LanguageTests()
        {
            _language = new Language();
        }

        [Test]
        public void TestSetAndGetId()
        {
            _language.Id = Id;
            Assert.That(_language.Id,
                Is.EqualTo(Id));
        }

        [Test]
        public void TestSetAndGetLanguageCultureName()
        {
            _language.LanguageCultureName = LanguageCultureName;
            Assert.That(_language.LanguageCultureName,
                Is.EqualTo(LanguageCultureName));
        }

        [Test]
        public void TestSetAndGetDisplayName()
        {
            _language.DisplayName = DisplayName;
            Assert.That(_language.DisplayName,
                Is.EqualTo(DisplayName));
        }
    }
}
