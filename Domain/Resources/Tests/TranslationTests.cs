﻿using NUnit.Framework;

namespace JustClassifieds.Domain.Resources.Tests
{
    [TestFixture]
    public class TranslationTests
    {
        private readonly Translation _translation;
        private const int Id = 1;
        private const string Key = "car-color-red";
        private const string Value = "rosu";
        private Language Language = new Language { Id = 1, LanguageCultureName = "ro-RO", DisplayName = "Romanian - Romania" };

        public TranslationTests()
        {
            _translation = new Translation();
        }

        [Test]
        public void TestSetAndGetId()
        {
            _translation.Id = Id;
            Assert.That(_translation.Id,
                Is.EqualTo(Id));
        }

        [Test]
        public void TestSetAndGetKey()
        {
            _translation.Key = Key;
            Assert.That(_translation.Key,
                Is.EqualTo(Key));
        }

        [Test]
        public void TestSetAndGetValue()
        {
            _translation.Value = Value;
            Assert.That(_translation.Value,
                Is.EqualTo(Value));
        }

        [Test]
        public void TestSetAndGetLanguage()
        {
            _translation.Language = Language;
            Assert.That(_translation.Language.Id, Is.EqualTo(Language.Id));
            Assert.That(_translation.Language.LanguageCultureName, Is.EqualTo(Language.LanguageCultureName));
            Assert.That(_translation.Language.DisplayName, Is.EqualTo(Language.DisplayName));
        }
    }
}
