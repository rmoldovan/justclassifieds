﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustClassifieds.Domain.Resources.Tests
{
    [TestFixture]
    public class CategoryTests
    {
        private readonly Category _category;
        private const int Id = 1;
        private const string Slug = "auto";
        private const string Key = "category-auto";

        public CategoryTests()
        {
            _category = new Category();
        }

        [Test]
        public void TestSetAndGetId()
        {
            _category.Id = Id;
            Assert.That(_category.Id,
                Is.EqualTo(Id));
        }

        [Test]
        public void TestSetAndGetSlug()
        {
            _category.Slug = Slug;
            Assert.That(_category.Slug,
                Is.EqualTo(Slug));
        }

        [Test]
        public void TestSetAndGetKey()
        {
            _category.Key = Key;
            Assert.That(_category.Key,
                Is.EqualTo(Key));
        }
    }
}
