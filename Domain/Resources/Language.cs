﻿using JustClassifieds.Domain.Common;

namespace JustClassifieds.Domain.Resources
{
    public class Language : BasicEntity<int>
    {
        public string LanguageCultureName { get; set; }
        public string DisplayName { get; set; }
    }
}
