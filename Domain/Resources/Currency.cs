﻿using JustClassifieds.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustClassifieds.Domain.Resources
{
    public class Currency : BasicEntity<int>
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string OfficialName { get; set; }
    }
}
