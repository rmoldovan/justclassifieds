﻿using JustClassifieds.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustClassifieds.Domain.Resources
{
    public class Category : BasicEntity<int>
    {
        public string Slug { get; set; }
        public string Key { get; set; }
        public virtual Category ParentCategory { get; set; }
    }
}
