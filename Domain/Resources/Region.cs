﻿using JustClassifieds.Domain.Common;

namespace JustClassifieds.Domain.Resources
{
    public class Region : BasicEntity<int>
    {
        public string Slug { get; set; }
        public string Key { get; set; }
        public bool IsCapital { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public virtual Region ParentRegion { get; set; }
    }
}
