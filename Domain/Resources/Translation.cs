﻿using JustClassifieds.Domain.Common;

namespace JustClassifieds.Domain.Resources
{
    public class Translation : BasicEntity<int>
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public virtual Language Language { get; set; }
    }
}
