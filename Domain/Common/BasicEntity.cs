﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustClassifieds.Domain.Common
{
    public abstract class BasicEntity<TKey>
    {
        public TKey Id { get; set; }
    }
}
