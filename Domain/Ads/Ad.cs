﻿using JustClassifieds.Domain.Common;
using System;

namespace JustClassifieds.Domain.Ads
{
    public class Ad : BasicEntity<Guid>
    {
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
