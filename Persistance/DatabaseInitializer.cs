﻿using System.Data.Entity;

namespace JustClassifieds.Persistance
{
    public class DatabaseInitializer
        : CreateDatabaseIfNotExists<DatabaseService>
    {
        protected override void Seed(DatabaseService database)
        {
            base.Seed(database);
        }
    }
}
