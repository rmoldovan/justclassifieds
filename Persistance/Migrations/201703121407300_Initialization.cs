namespace JustClassifieds.Persistance.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ads",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(nullable: false, maxLength: 127),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true, name: "UQ_ApplicationUser_Email");
            
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Slug = c.String(nullable: false, maxLength: 100),
                        Key = c.String(nullable: false, maxLength: 127),
                        ParentCategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.ParentCategory_Id)
                .Index(t => t.Slug, unique: true, name: "IX_Category_Slug")
                .Index(t => t.Key, unique: true, name: "IX_Category_Key")
                .Index(t => t.ParentCategory_Id);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                        ShortName = c.String(nullable: false, maxLength: 5),
                        OfficialName = c.String(nullable: false, maxLength: 5),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_Currency_Name")
                .Index(t => t.ShortName, unique: true, name: "IX_Currency_ShortName")
                .Index(t => t.OfficialName, unique: true, name: "IX_Currency_OfficialName");
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LanguageCultureName = c.String(nullable: false, maxLength: 10),
                        DisplayName = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.LanguageCultureName, unique: true, name: "IX_Language_CultureName")
                .Index(t => t.DisplayName, unique: true, name: "IX_Language_DisplayName");
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Slug = c.String(nullable: false, maxLength: 100),
                        Key = c.String(nullable: false, maxLength: 127),
                        IsCapital = c.Boolean(nullable: false),
                        Latitude = c.Double(),
                        Longitude = c.Double(),
                        ParentRegion_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.ParentRegion_Id)
                .Index(t => t.Slug, unique: true, name: "IX_Region_Slug")
                .Index(t => t.Key, unique: true, name: "IX_Region_Key")
                .Index(t => t.ParentRegion_Id);
            
            CreateTable(
                "dbo.Translations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 127),
                        Value = c.String(nullable: false, maxLength: 255),
                        Language_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Languages", t => t.Language_Id, cascadeDelete: true)
                .Index(t => new { t.Key, t.Value }, unique: true, name: "UQ_Translation")
                .Index(t => t.Language_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Translations", "Language_Id", "dbo.Languages");
            DropForeignKey("dbo.Regions", "ParentRegion_Id", "dbo.Regions");
            DropForeignKey("dbo.Categories", "ParentCategory_Id", "dbo.Categories");
            DropIndex("dbo.Translations", new[] { "Language_Id" });
            DropIndex("dbo.Translations", "UQ_Translation");
            DropIndex("dbo.Regions", new[] { "ParentRegion_Id" });
            DropIndex("dbo.Regions", "IX_Region_Key");
            DropIndex("dbo.Regions", "IX_Region_Slug");
            DropIndex("dbo.Languages", "IX_Language_DisplayName");
            DropIndex("dbo.Languages", "IX_Language_CultureName");
            DropIndex("dbo.Currencies", "IX_Currency_OfficialName");
            DropIndex("dbo.Currencies", "IX_Currency_ShortName");
            DropIndex("dbo.Currencies", "IX_Currency_Name");
            DropIndex("dbo.Categories", new[] { "ParentCategory_Id" });
            DropIndex("dbo.Categories", "IX_Category_Key");
            DropIndex("dbo.Categories", "IX_Category_Slug");
            DropIndex("dbo.ApplicationUsers", "UQ_ApplicationUser_Email");
            DropTable("dbo.Translations");
            DropTable("dbo.Regions");
            DropTable("dbo.Languages");
            DropTable("dbo.Currencies");
            DropTable("dbo.Categories");
            DropTable("dbo.Articles");
            DropTable("dbo.ApplicationUsers");
            DropTable("dbo.Ads");
        }
    }
}
