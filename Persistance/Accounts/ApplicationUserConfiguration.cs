﻿using JustClassifieds.Domain.Accounts;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Accounts
{
    public class ApplicationUserConfiguration
           : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            HasKey(u => u.Id);

            Property(u => u.Email)
                .IsRequired()
                .HasMaxLength(127)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_ApplicationUser_Email", 1) { IsUnique = true }));
        }
    }
}
