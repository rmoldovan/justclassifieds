﻿using JustClassifieds.Application.Interfaces;
using JustClassifieds.Domain.Accounts;
using JustClassifieds.Domain.Ads;
using JustClassifieds.Domain.Articles;
using JustClassifieds.Domain.Resources;
using JustClassifieds.Persistance.Accounts;
using JustClassifieds.Persistance.Ads;
using JustClassifieds.Persistance.Articles;
using JustClassifieds.Persistance.Resources;
using System.Data.Entity;

namespace JustClassifieds.Persistance
{
    public class DatabaseService : DbContext, IDatabaseService
    {
        public IDbSet<Category> Categories { get; set; }
        public IDbSet<Language> Languages { get; set; }
        public IDbSet<Translation> Translations { get; set; }
        public IDbSet<Region> Regions { get; set; }
        public IDbSet<Currency> Currencies { get; set; }
        public IDbSet<ApplicationUser> ApplicationUsers { get; set; }
        public IDbSet<Article> Articles { get; set; }
        public IDbSet<Ad> Ads { get; set; }

        public DatabaseService() : base("ClassifiedConnection")
        {
            Database.SetInitializer(new DatabaseInitializer());
        }

        public void Save()
        {
            this.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new LanguageConfiguration());
            modelBuilder.Configurations.Add(new TranslationConfiguration());
            modelBuilder.Configurations.Add(new RegionConfiguration());
            modelBuilder.Configurations.Add(new CurrencyConfiguration());
            modelBuilder.Configurations.Add(new ApplicationUserConfiguration());
            modelBuilder.Configurations.Add(new ArticleConfiguration());
            modelBuilder.Configurations.Add(new AdConfiguration());
        }
    }
}
