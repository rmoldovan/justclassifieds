﻿using JustClassifieds.Domain.Articles;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Articles
{
    public class ArticleConfiguration
           : EntityTypeConfiguration<Article>
    {
        public ArticleConfiguration()
        {
            HasKey(a => a.Id);
        }
    }
}
