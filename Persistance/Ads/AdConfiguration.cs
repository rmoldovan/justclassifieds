﻿using JustClassifieds.Domain.Ads;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Ads
{
    public class AdConfiguration
           : EntityTypeConfiguration<Ad>
    {
        public AdConfiguration()
        {
            HasKey(a => a.Id);
        }
    }
}
