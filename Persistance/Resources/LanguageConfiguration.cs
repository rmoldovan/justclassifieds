﻿using JustClassifieds.Domain.Resources;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Resources
{
    public class LanguageConfiguration
           : EntityTypeConfiguration<Language>
    {
        public LanguageConfiguration()
        {
            HasKey(l => l.Id);

            Property(l => l.LanguageCultureName)
                .IsRequired()
                .HasMaxLength(10)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Language_CultureName", 1) { IsUnique = true }));

            Property(l => l.DisplayName)
                .IsRequired()
                .HasMaxLength(30)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Language_DisplayName", 1) { IsUnique = true }));
        }
    }
}
