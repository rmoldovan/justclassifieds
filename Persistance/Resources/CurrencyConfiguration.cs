﻿using JustClassifieds.Domain.Resources;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Resources
{
    public class CurrencyConfiguration
           : EntityTypeConfiguration<Currency>
    {
        public CurrencyConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(30)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Currency_Name", 1) { IsUnique = true }));

            Property(c => c.ShortName)
                .IsRequired()
                .HasMaxLength(5)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Currency_ShortName", 1) { IsUnique = true }));

            Property(c => c.OfficialName)
                .IsRequired()
                .HasMaxLength(5)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Currency_OfficialName", 1) { IsUnique = true }));
        }
    }
}
