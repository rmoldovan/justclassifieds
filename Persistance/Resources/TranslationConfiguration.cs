﻿using JustClassifieds.Domain.Resources;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Resources
{
    public class TranslationConfiguration
           : EntityTypeConfiguration<Translation>
    {
        public TranslationConfiguration()
        {
            HasKey(t => t.Id);

            Property(t => t.Key)
                .IsRequired()
                .HasMaxLength(127)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_Translation", 1) { IsUnique = true }));

            Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(255)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_Translation", 2) { IsUnique = true }));

            HasRequired(t => t.Language);
        }
    }
}
