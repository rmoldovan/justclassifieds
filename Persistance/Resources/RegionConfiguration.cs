﻿using JustClassifieds.Domain.Resources;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Resources
{
    public class RegionConfiguration
           : EntityTypeConfiguration<Region>
    {
        public RegionConfiguration()
        {
            HasKey(r => r.Id);

            Property(r => r.Slug)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Region_Slug", 1) { IsUnique = true }));

            Property(r => r.Key)
                .IsRequired()
                .HasMaxLength(127)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Region_Key", 1) { IsUnique = true }));

            Property(r => r.IsCapital)
                .IsRequired();

            Property(r => r.Latitude)
                .IsOptional();

            Property(r => r.Longitude)
                .IsOptional();

            HasOptional(c => c.ParentRegion);
        }
    }
}
