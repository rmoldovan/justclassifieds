﻿using JustClassifieds.Domain.Resources;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace JustClassifieds.Persistance.Resources
{
    public class CategoryConfiguration
           : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Slug)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Category_Slug", 1) { IsUnique = true }));

            Property(c => c.Key)
                .IsRequired()
                .HasMaxLength(127)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Category_Key", 1) { IsUnique = true }));

            HasOptional(c => c.ParentCategory);
        }
    }
}
